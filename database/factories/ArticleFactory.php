<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use App\People;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'author_id' => function () {
            return factory(People::class)->create()->id;
        },
        'title' => $faker->sentence(),
    ];
});
